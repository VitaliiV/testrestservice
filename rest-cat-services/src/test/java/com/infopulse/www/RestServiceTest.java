package com.infopulse.www;

import com.infopulse.www.model.Cat;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import com.infopulse.www.exceptions.PetsException;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RestServiceTest {

    private static final int CAT_ID = 1;
    private static final String CAT_NAME = "name";
    private static final int CAT_AGE = 5;
    private static final Cat CAT = new Cat(CAT_ID, CAT_NAME, CAT_AGE);
    private static final int NOT_EXIST_CAT_ID = 0;

    private static RestService restService;

    @BeforeClass //before all method
    public static void initRestService() {

        DataBase dataBase = mock(DataBase.class);

        when(dataBase.getCatByID(CAT_ID)).thenReturn(CAT);
        when(dataBase.getCatByID(not(eq(CAT_ID)))).thenReturn(null);
        when(dataBase.addCat(CAT)).thenReturn(CAT_ID);
        List<Cat> listCat = new ArrayList<>();
        listCat.add(CAT);
        when(dataBase.getAllCats()).thenReturn(listCat);
        when(dataBase.editCat(CAT_ID, CAT)).thenReturn(true);
        when(dataBase.editCat(not(eq(CAT_ID)), eq(CAT))).thenReturn(false);
        when(dataBase.deleteCat(CAT_ID)).thenReturn(true);
        when(dataBase.deleteCat(not(eq(CAT_ID)))).thenReturn(false);

        restService = new RestService(dataBase);
    }

    @AfterClass //after all method
    public static void shutDown() {
        restService = null;
    }

    @Test
    public void addCatTest() {
        Cat cat = restService.addCat(CAT);
        verifyCat(cat, CAT_ID, CAT_NAME, CAT_AGE);
    }

    @Test
    public void getExistCatTest() {
        Cat cat = restService.getCat(CAT_ID);
        verifyCat(cat, CAT_ID, CAT_NAME, CAT_AGE);
    }

    @Test(expected = PetsException.class)
    public void getNotExistCatTest() {
        restService.getCat(NOT_EXIST_CAT_ID);
    }

    @Test
    public void getAllCatsTest() {
        List<Cat> catList = restService.getAllCats();
        assertNotNull(catList);
        assertEquals(1, catList.size());
        Cat cat = catList.get(0);
        verifyCat(cat, CAT_ID, CAT_NAME, CAT_AGE);
    }

    @Test
    public void editExistCatTest() {
        Cat cat = restService.editCat(CAT_ID, CAT);
        verifyCat(cat, CAT_ID, CAT_NAME, CAT_AGE);
    }

    @Test(expected = PetsException.class)
    public void editNotExistCatTest() {
        restService.editCat(NOT_EXIST_CAT_ID, CAT);
    }

    @Test
    public void deleteExistCatTest() {
        Response response = restService.deleteCat(CAT_ID);
        assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response.getStatus());
    }

    @Test(expected = PetsException.class)
    public void deleteNotExistCatTest() {
        restService.deleteCat(NOT_EXIST_CAT_ID);
    }

    private void verifyCat(Cat cat, int id, String name, int age) {
        assertNotNull(cat);
        assertEquals(id, cat.getId());
        assertEquals(name, cat.getName());
        assertEquals(age, cat.getAge());
    }
}
