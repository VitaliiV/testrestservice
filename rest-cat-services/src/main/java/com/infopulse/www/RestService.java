package com.infopulse.www;

import com.infopulse.www.model.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.infopulse.www.exceptions.PetsError;
import com.infopulse.www.exceptions.PetsException;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Service
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RestService {

    //private static final Logger LOGGER = LoggerFactory.getLogger(com.infopulse.www.RestService.class);

    private DataBase dataBase;

    public RestService() {
    }

    @Autowired
    public RestService(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    @GET
    @Path("/cats")
    public List<Cat> getAllCats() {
        //LOGGER.info("com.infopulse.www.RestService getAllCats()");
        return dataBase.getAllCats();
    }

    @GET
    @Path("/cats/{id}")
    public Cat getCat(@PathParam("id") int id) {
        //LOGGER.info("com.infopulse.www.RestService getCat(), id = " + id);
        Cat cat = dataBase.getCatByID(id);
        if (cat != null) {
            return cat;
        } else {
            throw new PetsException(PetsError.E0001_CAT_NOT_FOUND);
        }
    }

    @POST //add
    @Path("/cats")
    public Cat addCat(@Valid Cat cat) {
        //LOGGER.info("com.infopulse.www.RestService addCat()");
        int newCatId = dataBase.addCat(cat);
        return getCat(newCatId);
    }

    @PUT //edit
    @Path("/cats/{id}")
    public Cat editCat(@PathParam("id") int id, @Valid Cat cat) {
        //LOGGER.info("com.infopulse.www.RestService editCat(), id=" + id);
        boolean catIsExist = dataBase.editCat(id, cat);
        if (catIsExist) {
            return getCat(id);
        } else {
            throw new PetsException(PetsError.E0001_CAT_NOT_FOUND);
        }
    }

    @DELETE
    @Path("/cats/{id}")
    public Response deleteCat(@PathParam("id") int id) {
        //LOGGER.info("com.infopulse.www.RestService deleteCat(), id = " + id);
        if (dataBase.deleteCat(id)) {
            Response.Status status = Response.Status.NO_CONTENT;
            return Response.status(status).build();
        } else {
            throw new PetsException(PetsError.E0002_WRONG_CAT);
        }
    }

    public DataBase getDataBase() {
        return dataBase;
    }
}
