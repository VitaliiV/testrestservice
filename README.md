## README ##

TestRestService it is my training project.  
It provides the possibility to operate with cats :)

## Install

```
$ git clone https://VitaliiV@bitbucket.org/VitaliiV/testrestservice.git
$ cd rest-cat-parent
$ mvn install
$ cd rest-cat-parent/rest-cat-war
$ mvn jetty:run
```
application has two Spring profiles :  
StorageDB (default) - use Map like inmemory data base  
DerbyDB - use Derby embedded data base  
to activate DerbyDB profile:
```
$ mvn jetty:run -Dspring.profiles.active=DerbyDB
```
if you ned to start application on the custom port (8080 by default):
```
$ mvn -Djetty.port=<custom_port> jetty:run
```
or edit in pom.xml:
```
<jetty.port>8080</jetty.port>
```
## Testing in Postman
import to Postman collection:
testrestservice/test-postman/CatRestServiceTest.postman_collection.json

to run test on custom port, import Environment from  
"test-postman/catEnv.postman_environment.json"  
and edit "url" environment variable

## Testing in newman
Install Node.js, after install newman:
```
$ npm install -g newman
```
and run tests:
```
$ cd testrestservice/test-postman
$ newman run CatRestServiceTest.postman_collection.json
```
to run test on custom port, in "test-postman/catEnv.postman_environment.json" edit 
```
"value": "http://127.0.0.1:8080"
```
and run tests:
```
newman run CatRestServiceTest.postman_collection.json -e catEnv.postman_environment.json
```


## Examples of requests
POST request
```
POST http://127.0.0.1:8080/app/cats
{
  "name": "Tom",
  "age": 3
}
```
POST response
```
{
  "id": 1,
  "name": "Tom",
  "age": 3
}
```

GET request
```
http://127.0.0.1:8080/app/cats/1
```
GET response
```
{
  "id": 1,
  "name": "Tom",
  "age": 3
}
```

PUT request
```
http://127.0.0.1:8080/app/cats/1
{
	"name": "PHP",
	"age": 3
}
```
PUT response
```
{
  "id": 1,
  "name": "PHP",
  "age": 3
}
```
DELETE request
```
http://127.0.0.1:8080/app/cats/1
```
DELETE response
```
```