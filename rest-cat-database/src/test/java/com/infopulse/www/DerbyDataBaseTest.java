package com.infopulse.www;

import com.infopulse.www.model.Cat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/database-context.xml")
@ActiveProfiles(DataBase.DERBY_DB_PROFILE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DerbyDataBaseTest {

    private static final String INSERT_CAT_SQL = "INSERT INTO CATS (NAME, AGE) VALUES (:NAME, :AGE)";
    private static final String GET_CAT_BY_ID = "SELECT * FROM CATS WHERE ID = :ID";

    private static final int NOT_EXIST_CAT_ID = 0;
    private static final String NEW_CAT_NAME = "cat";
    private static final int NEW_CAT_AGE = 10;
    private static final String CAT_NAME = "name";
    private static final int CAT_AGE = 5;

    private static int CAT_ID = 1;

    private static RowMapper<Cat> catRowMapper = DerbyDataBase.getCatRowMapper();

    @Autowired
    private DerbyDataBase derbyDataBase;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    public DerbyDataBaseTest() {
    }

//    @BeforeClass
//    public static void initStorageDataBase() throws Exception {
//
//        FileSystemXmlApplicationContext ctx = new FileSystemXmlApplicationContext(
//                "src\\main\\webapp\\WEB-INF\\database-context.xml");
//
//        ConfigurableEnvironment env = ctx.getEnvironment();
//        env.setActiveProfiles("DerbyDB");
//        ctx.refresh();
//
//        dataSource = (DriverManagerDataSource) ctx.getBean("dataSource");
//        derbyDataBase = (DerbyDataBase) ctx.getBean("derbyDataBase");
//        derbyDataBase.setDataSource(dataSource);
//
//        dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName("org.apache.derby.jdbc.EmbeddedDriver");
//        dataSource.setUrl("jdbc:derby:sampleDB;create=true");
//        derbyDataBase = new DerbyDataBase(dataSource);
//    }

    @Before
    public void initCat() throws Exception {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("NAME", CAT_NAME);
        namedParameters.put("AGE", CAT_AGE);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource paramSource = new MapSqlParameterSource(namedParameters);
        namedParameterJdbcTemplate.update(INSERT_CAT_SQL, paramSource, keyHolder);
        CAT_ID = keyHolder.getKey().intValue();
    }

    @Test
    public void addCatTest() throws Exception {
        int generatedId = derbyDataBase.addCat(new Cat(NEW_CAT_NAME, NEW_CAT_AGE));
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("ID", generatedId);
        Cat cat = namedParameterJdbcTemplate.queryForObject(GET_CAT_BY_ID, namedParameters, catRowMapper);
        verifyCat(cat, generatedId, NEW_CAT_NAME, NEW_CAT_AGE);
    }

    @Test
    public void getExistCat() throws Exception {
        Cat dbCat = derbyDataBase.getCatByID(CAT_ID);
        verifyCat(dbCat, CAT_ID, CAT_NAME, CAT_AGE);
    }

    @Test
    public void getNotExistCat() throws Exception {
        Cat dbCat = derbyDataBase.getCatByID(NOT_EXIST_CAT_ID);
        assertNull(dbCat);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void deleteExistCat() throws Exception {
        assertTrue(derbyDataBase.deleteCat(CAT_ID));
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("ID", CAT_ID);
        namedParameterJdbcTemplate.queryForObject(GET_CAT_BY_ID, namedParameters, catRowMapper);
    }

    @Test
    public void deleteNotExistCat() throws Exception {
        assertFalse(derbyDataBase.deleteCat(NOT_EXIST_CAT_ID));
    }

    @Test
    public void editExistCat() throws Exception {
        assertTrue(derbyDataBase.editCat(CAT_ID, new Cat(NEW_CAT_NAME, NEW_CAT_AGE)));

        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("ID", CAT_ID);
        Cat cat = namedParameterJdbcTemplate.queryForObject(GET_CAT_BY_ID, namedParameters, catRowMapper);
        verifyCat(cat, CAT_ID, NEW_CAT_NAME, NEW_CAT_AGE);
    }

    @Test
    public void editNotExistCat() throws Exception {
        assertFalse(derbyDataBase.editCat(NOT_EXIST_CAT_ID, new Cat(NEW_CAT_NAME, NEW_CAT_AGE)));
    }

    @Test
    public void getAllCats() throws Exception {
        List<Cat> catList = derbyDataBase.getAllCats();
        assertNotNull(catList);
        assertEquals(1, catList.size());
        Cat dbCat = catList.get(0);
        verifyCat(dbCat, CAT_ID, CAT_NAME, CAT_AGE);
    }

    private void verifyCat(Cat cat, int id, String name, int age) {
        assertNotNull(cat);
        assertEquals(id, cat.getId());
        assertEquals(name, cat.getName());
        assertEquals(age, cat.getAge());
    }
}