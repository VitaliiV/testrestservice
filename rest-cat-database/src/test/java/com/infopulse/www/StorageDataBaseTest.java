package com.infopulse.www;

import com.infopulse.www.model.Cat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/database-context.xml")
@ActiveProfiles(DataBase.STORAGE_DB_PROFILE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class StorageDataBaseTest {

    private static final int NOT_EXIST_CAT_ID = 0;
    private static final int CAT_ID = 1;
    private static final String CAT_NAME = "name";
    private static final int CAT_AGE = 5;
    private static final String NEW_CAT_NAME = "cat";
    private static final int NEW_CAT_AGE = 10;

    @Autowired
    private StorageDataBase storageDataBase;

    @Before
    public void init() {
        storageDataBase.clear();
        storageDataBase.getCatStorage().put(CAT_ID, new Cat(CAT_ID, CAT_NAME, CAT_AGE));
    }

    @Test
    public void addCatTest() throws Exception {
        int generatedId = storageDataBase.addCat(new Cat(NEW_CAT_NAME, NEW_CAT_AGE));
        Cat dbCat = storageDataBase.getCatStorage().get(generatedId);
        verifyCat(dbCat, generatedId, NEW_CAT_NAME, NEW_CAT_AGE);
    }

    @Test
    public void getExistCat() throws Exception {
        Cat dbCat = storageDataBase.getCatByID(CAT_ID);
        verifyCat(dbCat, CAT_ID, CAT_NAME, CAT_AGE);
    }

    @Test
    public void getNotExistCat() throws Exception {
        Cat dbCat = storageDataBase.getCatByID(NOT_EXIST_CAT_ID);
        assertNull(dbCat);
    }

    @Test
    public void deleteExistCat() throws Exception {
        assertTrue(storageDataBase.deleteCat(CAT_ID));
        assertEquals(0, storageDataBase.getCatStorage().size());
    }

    @Test
    public void deleteNotExistCat() throws Exception {
        assertFalse(storageDataBase.deleteCat(NOT_EXIST_CAT_ID));
    }

    @Test
    public void editExistCat() throws Exception {
        assertTrue(storageDataBase.editCat(CAT_ID, new Cat(NEW_CAT_NAME, NEW_CAT_AGE)));
        Cat dbCat = storageDataBase.getCatStorage().get(CAT_ID);
        verifyCat(dbCat, CAT_ID, NEW_CAT_NAME, NEW_CAT_AGE);
    }

    @Test
    public void editNotExistCat() throws Exception {
        assertFalse(storageDataBase.editCat(NOT_EXIST_CAT_ID, new Cat(NEW_CAT_NAME, NEW_CAT_AGE)));
    }

    @Test
    public void getAllCats() throws Exception {
        List<Cat> catList = storageDataBase.getAllCats();
        assertNotNull(catList);
        assertEquals(1, catList.size());
        Cat dbCat = catList.get(0);
        verifyCat(dbCat, CAT_ID, CAT_NAME, CAT_AGE);
    }

    @Test
    public void size() throws Exception {
        assertEquals(1, storageDataBase.size());
    }

    @Test
    public void clear() throws Exception {
        storageDataBase.clear();
        assertEquals(0, storageDataBase.getCatStorage().size());
        assertEquals(0, storageDataBase.getIdCounter());
    }

    private void verifyCat(Cat cat, int id, String name, int age) {
        assertNotNull(cat);
        assertEquals(id, cat.getId());
        assertEquals(name, cat.getName());
        assertEquals(age, cat.getAge());
    }

    public StorageDataBase getStorageDataBase() {
        return storageDataBase;
    }

    public void setStorageDataBase(StorageDataBase storageDataBase) {
        this.storageDataBase = storageDataBase;
    }
}