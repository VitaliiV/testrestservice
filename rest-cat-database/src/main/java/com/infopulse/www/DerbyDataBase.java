package com.infopulse.www;

import com.infopulse.www.model.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@Service
//@Configuration
//@Profile("DerbyDB")
public class DerbyDataBase implements DataBase {

    private static final String INSERT_CAT_SQL = "INSERT INTO CATS (NAME, AGE) VALUES (:NAME, :AGE)";
    private static final String GET_CAT_BY_ID = "SELECT * FROM CATS WHERE ID = :ID";
    private static final String UPDATE_CAT_WITH_ID = "UPDATE CATS SET NAME = :NAME, AGE = :AGE WHERE ID = :ID";
    private static final String DELETE_CAT_WHERE_ID = "DELETE FROM CATS WHERE ID = :ID";
    private static final String SELECT_ALL_CATS = "SELECT * FROM CATS";

    private static RowMapper<Cat> catRowMapper = (rs, rowNum) -> {
        Cat cat = new Cat();
        cat.setId(rs.getInt("ID"));
        cat.setName(rs.getString("NAME"));
        cat.setAge(rs.getInt("AGE"));
        return cat;
    };

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public DerbyDataBase() {
    }

    @Autowired
    public DerbyDataBase(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public int addCat(Cat cat) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("NAME", cat.getName());
        namedParameters.put("AGE", cat.getAge());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource paramSource = new MapSqlParameterSource(namedParameters);
        namedParameterJdbcTemplate.update(INSERT_CAT_SQL, paramSource, keyHolder);
        return keyHolder.getKey().intValue();
    }

    @Override
    public Cat getCatByID(int id) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("ID", id);
        try {
            return namedParameterJdbcTemplate.queryForObject(GET_CAT_BY_ID, namedParameters, catRowMapper);
        } catch (EmptyResultDataAccessException exception) {
            return null;
        }
    }

    @Override
    public boolean editCat(int id, Cat newCat) {
        Cat dbCat = getCatByID(id);
        if (dbCat == null) {
            return false;
        } else {
            int newAge = newCat.getAge();
            String newName = newCat.getName();
            if (newAge != 0) {
                dbCat.setAge(newAge);
            }
            if (newName != null) {
                dbCat.setName(newName);
            }
            Map<String, Object> namedParameters = new HashMap<>();
            namedParameters.put("NAME", newCat.getName());
            namedParameters.put("AGE", newCat.getAge());
            namedParameters.put("ID", id);
            namedParameterJdbcTemplate.update(UPDATE_CAT_WITH_ID, namedParameters);
            return true;
        }
    }

    @Override
    public boolean deleteCat(int id) {
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("ID", id);
        int result = namedParameterJdbcTemplate.update(DELETE_CAT_WHERE_ID, namedParameters);
        return result == 1;
    }

    @Override
    public List<Cat> getAllCats() {
        return namedParameterJdbcTemplate.query(SELECT_ALL_CATS, catRowMapper);
    }

//    private static class CatMapper implements RowMapper {
//        public Cat mapRow(ResultSet rs, int rowNum) throws SQLException {
//            Cat cat = new Cat();
//            cat.setId(rs.getInt("ID"));
//            cat.setName(rs.getString("NAME"));
//            cat.setAge(rs.getInt("AGE"));
//            return cat;
//        }
//    }


    public static RowMapper<Cat> getCatRowMapper() {
        return catRowMapper;
    }
}
