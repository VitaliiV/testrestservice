package com.infopulse.www;

import com.infopulse.www.model.Cat;

import javax.annotation.PostConstruct;
import java.util.*;

//@Service
//@Configuration
//@Profile("StorageDB")
public class StorageDataBase implements DataBase {

    private Map<Integer, Cat> catStorage = new HashMap<>();
    private int idCounter;

    @Override
    public int addCat(Cat cat) {
        cat.setId(++idCounter);
        catStorage.put(cat.getId(), cat);
        return cat.getId();
    }

    @Override
    public Cat getCatByID(int id) {
        return catStorage.get(id);
    }

    @Override
    public boolean deleteCat(int id) {
        Cat removeCat = catStorage.remove(id);
        return removeCat != null;
    }

    @Override
    public boolean editCat(int id, Cat cat) {
        if (catStorage.containsKey(id)) {
            Cat storageCat = catStorage.get(id);
            int newAge = cat.getAge();
            String newName = cat.getName();
            if (newAge != 0) {
                storageCat.setAge(newAge);
            }
            if (newName != null) {
                storageCat.setName(newName);
            }
            return true;
        }
        return false;
    }

    @Override
    public List<Cat> getAllCats() {
        List<Cat> catList = new ArrayList<Cat>();
        Set<Integer> keySet = catStorage.keySet();
        for (int key : keySet) {
            catList.add(catStorage.get(key));
        }
        return catList;
    }

    public int size() {
        return catStorage.size();
    }

    public void clear() {
        catStorage.clear();
        idCounter = 0;
    }

    public int getIdCounter() {
        return idCounter;
    }

    public Map<Integer, Cat> getCatStorage() {
        return catStorage;
    }


}
