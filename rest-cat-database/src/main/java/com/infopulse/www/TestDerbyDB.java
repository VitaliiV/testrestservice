package com.infopulse.www;

import java.sql.*;

public class TestDerbyDB {


    public static void main(String[] args) {
        try {
            Connection connection = createDatabaseConnection();

            Statement statement = connection.createStatement();

//            int result1 = statement.executeUpdate("DROP TABLE cats");
//            System.out.println(result1);
//            int result2 = statement.executeUpdate("CREATE TABLE cats (id INT PRIMARY KEY NOT NULL, name CHAR(20), age INTEGER )");
//            System.out.println(result2);
//            int result3 = statement.executeUpdate("INSERT INTO cats (id, name, age) VALUES (1, 'cat1', 5)");
//            System.out.println(result3);
//            int result4 = statement.executeUpdate("INSERT INTO cats (id, name, age) VALUES (2, 'cat2', 6)");
//            System.out.println(result3);
//            int result5 = statement.executeUpdate("INSERT INTO cats (id, name, age) VALUES (3, 'cat3', 7)");
//            System.out.println(result3);

            ResultSet resultSet = statement.executeQuery("SELECT * FROM cats");

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int age = resultSet.getInt("age");
                System.out.println("Cat{id=" + id + ", name=" + name + ", age=" + age + "}");
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            //System.out.println(e);
        }
    }

    private static Connection createDatabaseConnection()
            throws SQLException, ClassNotFoundException {
        String driver = "org.apache.derby.jdbc.EmbeddedDriver";
        Class.forName(driver);
        String url = "jdbc:derby:sampleDB;create=true";
        return DriverManager.getConnection(url);
    }
}



//    String query = "select * from people where (first_name = :name or last_name
//            = :name) and address = :address");
//    NamedParameterStatement p = new NamedParameterStatement(con, query);
//    p.setString("name", name);
//    p.setString("address", address);