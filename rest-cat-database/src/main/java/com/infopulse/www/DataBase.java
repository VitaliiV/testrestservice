package com.infopulse.www;

import com.infopulse.www.model.Cat;

import java.util.List;

public interface DataBase{

    public static final String STORAGE_DB_PROFILE = "StorageDB";
    public static final String DERBY_DB_PROFILE = "DerbyDB";
    public static final String SOAP_DB_PROFILE = "SoapDB";

    int addCat(Cat cat);

    Cat getCatByID(int id);

    boolean editCat(int id, Cat cat);

    boolean deleteCat(int id);

    List<Cat> getAllCats();
}
