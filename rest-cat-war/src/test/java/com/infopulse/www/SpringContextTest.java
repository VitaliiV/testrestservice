package com.infopulse.www;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;

import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

public class SpringContextTest {

//    @Autowired
//    ConfigurableApplicationContext applicationContext;

    private static final String EXCEPTION_MESSAGE = "Exception: Spring context initialization in profile: ";
    private static final String BEANS_EXCEPTION_MESSAGE = "BeansException: Spring Context initialization fail in profile: ";
    private static final String SPRING_CONTEXT_LOCATION = "*-applicationContext.xml";

    //TODO not work, ned fix!
    private static final Logger LOG = LoggerFactory.getLogger(SpringContextTest.class);

    @Test
    public void contextStorageDBTest() {
        contextInitTest(DataBase.STORAGE_DB_PROFILE);
    }

    @Test
    public void contextDerbyDBTest() {
        contextInitTest(DataBase.DERBY_DB_PROFILE);
    }

    @Test
    public void contextSoapDBTest() {
        contextInitTest(DataBase.SOAP_DB_PROFILE);
    }

    private void contextInitTest(String profile) {
        try {
            GenericXmlApplicationContext context = new GenericXmlApplicationContext();
            ConfigurableEnvironment  environment= context.getEnvironment();
            environment.addActiveProfile(profile);
            context.load(SPRING_CONTEXT_LOCATION);
            context.refresh();
            context.close();
        } catch (BeansException e) {
            Assert.fail(BEANS_EXCEPTION_MESSAGE + profile);
            LOG.error(BEANS_EXCEPTION_MESSAGE + profile, e);
        } catch (Exception e) {
            LOG.error(EXCEPTION_MESSAGE + profile, e);
            Assert.fail(EXCEPTION_MESSAGE + profile);
        }
    }
}