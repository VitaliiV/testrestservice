package com.infopulse.www.exceptionhendlers;

import com.infopulse.www.exceptions.ErrorMessageEntity;
import com.infopulse.www.exceptions.PetsException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class PetsExceptionHandler implements ExceptionMapper<PetsException> {

    //private static final Logger LOGGER = LoggerFactory.getLogger(ServerExceptionHandler.class);

    public Response toResponse(PetsException exception) {

        //LOGGER.error(exception.toString());

        ErrorMessageEntity message = new ErrorMessageEntity();
        message.setMessage(exception.getMessage());

        return Response.status(exception.getStatusCode())
                .type(MediaType.APPLICATION_JSON)
                .entity(message)
                .build();
    }
}