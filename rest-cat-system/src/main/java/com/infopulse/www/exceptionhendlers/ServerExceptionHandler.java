package com.infopulse.www.exceptionhendlers;

import com.infopulse.www.exceptions.ErrorMessageEntity;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class ServerExceptionHandler implements ExceptionMapper<Exception> {

    //private static final Logger LOGGER = LoggerFactory.getLogger(ServerExceptionHandler.class);

    public Response toResponse(Exception exception) {

        //LOGGER.error(exception.toString());

        ErrorMessageEntity messageEntity = new ErrorMessageEntity();
        messageEntity.setMessage(exception.toString());
        addCauseToList(exception, messageEntity);

        Response.StatusType status;
        //if(WebApplicationException.class.isAssignableFrom(exception.getClass())){
        if (exception instanceof WebApplicationException) {
            status = ((WebApplicationException) exception).getResponse().getStatusInfo();
        } else {
            status = Response.Status.INTERNAL_SERVER_ERROR;
        }

        return Response.status(status)
                //Response.status(exception.getResponse().getStatusInfo())
                .type(MediaType.APPLICATION_JSON)
                .entity(messageEntity)
                .build();
    }

    private void addCauseToList(Throwable exception, ErrorMessageEntity messageEntity) {
        Throwable cause = exception.getCause();
        if (cause != null) {
            messageEntity.getCausedByList().add(cause.toString());
            addCauseToList(cause, messageEntity);
        }
    }
}
