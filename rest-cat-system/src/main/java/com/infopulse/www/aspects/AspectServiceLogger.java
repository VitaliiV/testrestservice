package com.infopulse.www.aspects;

import com.infopulse.www.exceptions.PetsException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Aspect
@Service
public class AspectServiceLogger {

    private static final Logger LOGGER = LoggerFactory.getLogger(AspectServiceLogger.class);

    @Pointcut("execution(* com.infopulse.www.RestService.*(..))")
    public void callRestServiceAnyMethod() {
    }

    @Pointcut("@annotation(javax.ws.rs.Path)")
    public void callRestServicePathMethods() {
    }

    @Pointcut("execution(* com.infopulse.www.exceptionhendlers.PetsExceptionHandler.toResponse(..))")
    public void callPetsExceptionHandler() {
    }

    @Pointcut("execution(* com.infopulse.www.exceptionhendlers.ServerExceptionHandler.toResponse(..))")
    public void callServerExceptionHandler() {
    }

    @Before("callRestServiceAnyMethod() && callRestServicePathMethods()")
    public void logRestServiceCall(JoinPoint joinPoint) {
        if (LOGGER.isDebugEnabled()) {
            StringBuilder sb = new StringBuilder();
            sb.append("RestService ")
                    .append(joinPoint.getSignature().getName())
                    .append("(");
            Object[] args = joinPoint.getArgs();
            boolean isFirst = true;
            for (Object arg : args) {
                if (isFirst) {
                    sb.append(arg);
                } else {
                    sb.append(",");
                    sb.append(arg);
                }
                isFirst = false;
            }
            sb.append(")");
            LOGGER.debug(sb.toString());
        }
    }

    @Before("callServerExceptionHandler()")
    public void logServerExceptionHandlerCall(JoinPoint joinPoint) {
        Exception exception = (Exception) joinPoint.getArgs()[0];
        LOGGER.error("Exception: ", exception);
    }

    @Before("callPetsExceptionHandler()")
    public void logPetsExceptionHandlerCall(JoinPoint joinPoint) {
        PetsException exception = (PetsException) joinPoint.getArgs()[0];
        LOGGER.error("PetsException: {}, response status code: {}",
                exception.getMessage(),
                exception.getStatusCode());
    }

}