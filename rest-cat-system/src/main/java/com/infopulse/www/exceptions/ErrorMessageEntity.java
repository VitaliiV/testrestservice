package com.infopulse.www.exceptions;

import java.util.ArrayList;
import java.util.List;

public class ErrorMessageEntity {

    private String message;
    private List<String> causedByList = new ArrayList<>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getCausedByList() {
        return causedByList;
    }

    public void setCausedByList(List<String> causedBy) {
        this.causedByList = causedBy;
    }
}