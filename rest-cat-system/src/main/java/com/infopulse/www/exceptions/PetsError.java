package com.infopulse.www.exceptions;

import javax.ws.rs.core.Response;

public enum PetsError {

    E0001_CAT_NOT_FOUND(Response.Status.NOT_FOUND, "No cat with this ID"),
    E0002_WRONG_CAT(Response.Status.BAD_REQUEST, "Wrong cat id ID"),
    E0003_UNEXPECTED_ID(Response.Status.BAD_REQUEST, "You can not specify custom ID");

    private Response.Status httpStatus;
    private String message;

    PetsError(Response.Status httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public Response.Status getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }
}
