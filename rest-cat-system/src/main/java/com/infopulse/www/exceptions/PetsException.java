package com.infopulse.www.exceptions;

import javax.ws.rs.core.Response;

public class PetsException extends RuntimeException {

    private Response.Status statusCode;

    public PetsException(PetsError error) {
        super(error.getMessage());
        statusCode = error.getHttpStatus();
    }

    public PetsException(PetsError error, Throwable cause) {
        super(error.getMessage(), cause);
        statusCode = error.getHttpStatus();
    }

    public Response.Status getStatusCode() {
        return statusCode;
    }
}
