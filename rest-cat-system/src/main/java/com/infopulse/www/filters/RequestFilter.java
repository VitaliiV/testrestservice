package com.infopulse.www.filters;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@PreMatching
public class RequestFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        final URI absolutePath = containerRequestContext.getUriInfo().getAbsolutePath();
        String path = absolutePath.getPath();
        if (path.contains("/cat_store")) {
            path = path.replace("/cat_store", "/cats");
        }
        try {
            containerRequestContext.setRequestUri(new URI(path));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}