package com.infopulse.www.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.infopulse.www.exceptions.PetsError;
import com.infopulse.www.exceptions.PetsException;
import com.infopulse.www.model.Cat;

import java.io.IOException;

public class CatDeserializer extends StdDeserializer<Cat> {

    public CatDeserializer() {
        this(null);
    }

    public CatDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Cat deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException, PetsException {

        JsonNode node = jp.getCodec().readTree(jp);
        JsonNode idNode = node.get("id");
        if (idNode != null) {
            throw new PetsException(PetsError.E0003_UNEXPECTED_ID);
        }

        JsonNode nameNode = node.get("name");
        String name = null;
        if (nameNode != null) {
            name = node.get("name").asText();
        }

        JsonNode ageNode = node.get("age");
        int age = 0;
        if (ageNode != null) {
            age = (Integer) node.get("age").numberValue();
        }

        return new Cat(name, age);
    }
}
