package com.infopulse.www;

import com.infopulse.www.aspects.AspectServiceLogger;
import com.infopulse.www.exceptions.PetsException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.infopulse.www.exceptions.PetsError;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggerFactory.class})
public class AspectServiceLoggerTest {

    private static AspectServiceLogger aspectServiceLogger;
    private static Logger mockedLogger;

    @BeforeClass
    public static void initRestService() {
        mockStatic(LoggerFactory.class);
        mockedLogger = mock(Logger.class);
        when(LoggerFactory.getLogger(any(Class.class))).thenReturn(mockedLogger);
        when(mockedLogger.isDebugEnabled()).thenReturn(true);
        when(mockedLogger.isErrorEnabled()).thenReturn(true);
        aspectServiceLogger = new AspectServiceLogger();
    }

    @Test
    public void logRestServiceCallTest() throws Exception {
        JoinPoint joinPoint = Mockito.mock(JoinPoint.class);
        Signature signature = Mockito.mock(Signature.class);
        when(joinPoint.getSignature()).thenReturn(signature);
        when(joinPoint.getArgs()).thenReturn(new Object[0]);

        aspectServiceLogger.logRestServiceCall(joinPoint);
        verify(mockedLogger).debug(anyString());
    }

    @Test
    public void logServerExceptionHandlerCallTest() throws Exception {
        JoinPoint joinPoint = Mockito.mock(JoinPoint.class);
        Object[] array = {new Exception()};
        when(joinPoint.getArgs()).thenReturn(array);

        aspectServiceLogger.logServerExceptionHandlerCall(joinPoint);
        verify(mockedLogger).error(anyString(), any(Exception.class));
    }

    @Test
    public void logPetsExceptionHandlerCallTest() throws Exception {
        JoinPoint joinPoint = Mockito.mock(JoinPoint.class);
        Object[] array = {new PetsException(PetsError.E0001_CAT_NOT_FOUND)};
        when(joinPoint.getArgs()).thenReturn(array);

        aspectServiceLogger.logPetsExceptionHandlerCall(joinPoint);
        verify(mockedLogger).error(anyString(), anyObject(), anyObject());
    }

}